let input = "1.09 4 9.86 10 9.86 10 9.86 10 9.86 10";
let wheelbase = parseFloat(input.split(" ")[0]);

let loop = parseInt(input.split(" ")[1])
let inputArr = input.split(" ")
inputArr.splice(0, 2);
let obArray = [];
for (let i = 0; i < loop; i++) {
    obArray.push({
        distance: parseFloat(inputArr[i * 2]),
        steeringAngle: parseFloat(inputArr[i * 2 + 1])
    })
}
// let distance = parseFloat(input.split(" ")[1]);
let resultArray = [];
for (let i = 0; i < loop; i++) {
    let el = obArray[i];
    let steeringAngle = el.steeringAngle



    let distance = el.distance
    let angle
    console.log(steeringAngle);
    if (steeringAngle === 0) {

        x = (0).toFixed(2);
        y = distance.toFixed(2)
        angle = resultArray[resultArray.length - 1].newDirection || 0;
        x = Math.cos(degrees_to_radians(angle)) * x + Math.sin(degrees_to_radians(angle)) * y;
        y = (Math.sin(degrees_to_radians(angle)) * -1) * x + Math.cos(degrees_to_radians(angle)) * y;
        console.log("new vector", { x: x, y: y });
    } else {
        if (i > 0) {
            // angle += resultArray[i-1].newDirection
            // steeringAngle += 15
        }
        console.log("Wheelbase:", wheelbase);
        let radius = wheelbase / Math.sin(degrees_to_radians(steeringAngle));

        console.log(radius)
        // radius = radius.toFixed(2);

        // b =( r * pi * a )/ 180


        angle = (distance * 180) / (Math.PI * radius)
        

        console.log(angle, radius);

        // console.log(angle)

        // Gk/H = tan(a)

        // Ak/H = cos(a)

        y = (Math.sin(degrees_to_radians(angle)) * radius)

        x = (radius - (Math.cos(degrees_to_radians(angle)) * radius))
        
        console.log("before rottatin", {x, y})
        if (i > 0) {
            // let angle2 = -angle;
            // x = Math.cos(degrees_to_radians(angle2)) * x + Math.sin(degrees_to_radians(angle2)) * y;
            // y = (Math.sin(degrees_to_radians(angle2)) * -1) * x + Math.cos(degrees_to_radians(angle2)) * y;
        }
        console.log("after rotating", {x, y})
        if (resultArray.length > 0) {
            console.log("angle before adding", angle)
            angle = angle + resultArray[resultArray.length - 1].newDirection;
        }
        console.log("x and y", x, y);
    }

    while (angle < 0 || angle > 360) {
        if (angle < 0) {
            angle += 360
        }
        else if (angle > 360) {
            angle -= 360
        }
    }
    if (resultArray.length > 0) {
        console.log("x before adding", x);
        console.log("y before adding", y);
        x = x + resultArray[resultArray.length - 1].x;
        y = y + resultArray[resultArray.length - 1].y;
    }
    x = round(x);
    y = round(y);
    angle = round(angle);
    resultArray.push({ x, y, newDirection: angle })

}
console.log(resultArray);

// console.log(x, y, angle.toFixed(2))
function degrees_to_radians(degrees) {
    var pi = Math.PI;
    return degrees * (pi / 180);
}

function round(num) {
    return Math.round(num * 100) / 100
}
