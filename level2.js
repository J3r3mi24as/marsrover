let input = "1.00 1.00 30.00";
let wheelbase = parseFloat(input.split(" ")[0]);

let distance = parseFloat(input.split(" ")[1]);

let steeringAngle = parseFloat(input.split(" ")[2])
let angle
if(steeringAngle === 0) {
    x = (0).toFixed(2);
    y = distance.toFixed(2)
    angle = 0
}else {
    
    let radius = wheelbase / Math.sin(degrees_to_radians(steeringAngle));
    
    console.log(radius)
    
    angle = (distance * 180) / (Math.PI * radius)
    
    console.log(angle)
    
    y = (Math.sin(degrees_to_radians(angle)) * radius).toFixed(2);
    
    x = (radius - (Math.cos(degrees_to_radians(angle)) * radius)).toFixed(2)
}


while(angle < 0 || angle > 360 ) {
    if(angle < 0) {
        angle += 360
    }
    else if(angle > 360) {
        angle -= 360
    }
}
console.log(x, y, angle.toFixed(2))
function degrees_to_radians(degrees)
{
  var pi = Math.PI;
  return degrees * (pi/180);
}
